import { TMetricsTypes } from '../../screens/HomeScreen/interfaces';

interface DataPoint {
  x: number;
  y: number;
}

export interface IProps {
  metrics: DataPoint[];
  getMetricsApi: () => void;
  type: TMetricsTypes;
}
