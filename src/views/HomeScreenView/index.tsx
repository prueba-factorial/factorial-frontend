import React, { ReactElement, useEffect, useState } from 'react';
import LinealChart from './Components/LinealCharter';
import { IProps } from './interfaces';
import AddMetric from './Components/AddMetric';
import FormSearchMetrics from './Components/FormSearchMetrics';
import ActionsMetrics from './Components/ActionsMetrics';

const HomeScreenView = ({ metrics, getMetricsApi, type }: IProps): ReactElement => {
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);
  const [withLinealChart, setWithLinealChart] = useState<number>(1200);

  const data = [{ data: metrics, id: 'metrics' }];

  const handleResize = (): void => {
    setScreenWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    const width = (screenWidth > 1400) ? 1200 : screenWidth - 200;
    setWithLinealChart(width);
  }, [screenWidth]);

  return (
    <div>
      <div className={ 'row' }>
        <div className={ 'col-lg-6 col-md-12 col-sm-12' }>
          <FormSearchMetrics getMetrics={ getMetricsApi } />
        </div>
        <div className={ 'col-lg-6 col-md-12 col-sm-12' }>
          <ActionsMetrics getMetrics={ getMetricsApi } />
        </div>
      </div>

      <div style={ { paddingTop: '50px' } }>
        <LinealChart
          data={ data }
          width={ withLinealChart }
          height={ 400 }
          min={ 'auto' }
          max={ 'auto' }
          type={ type }
        />
      </div>
      <div className={ 'box-add-metric' }>
        <AddMetric getMetrics={ getMetricsApi } />
      </div>
    </div>
  );
};

export default HomeScreenView;
