import { IDataSearchMetric, TMetricsTypes } from '../../../../screens/HomeScreen/interfaces';

export interface IInitialState {
  initDate: {
    value: string;
    error: string | null;
  };
  initTimePicker: {
    value: string;
    error: string | null;
  };
  endDate: {
    value: string;
    error: string | null;
  };
  type: {
    value: TMetricsTypes;
    error: string | null;
  };
}

export interface IProps {
  getMetrics: (data: IDataSearchMetric) => void;
}
