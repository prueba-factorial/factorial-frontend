import React, { useEffect } from 'react';
import Calendar from '../../../../components/Calendar';
import { useForm } from '../../../../hooks/useForm';
import formValidators from './utils/formValidators';
import { IInitialState, IProps } from './interfaces';
import SelectHour from '../../../../components/SelectHour';

import './formSearchMetrics.styles.css';
import { transformDateSearch } from './utils/transformDateSearch';
import { IDataSearchMetric } from '../../../../screens/HomeScreen/interfaces';

const initialState: IInitialState = {
  initDate: { value: '2023-11-01', error: null },
  initTimePicker: { value: '', error: null },
  endDate: { value: '2023-11-10', error: null },
  type: { value: 'day', error: null }
};
const FormSearchMetrics = ({ getMetrics }: IProps): React.ReactElement => {
  const {
    form, setFormValue, submitFn, submitOk, setSubmitOk
  } = useForm(initialState, formValidators);

  useEffect(() => {
    if (!submitOk) return;
    setSubmitOk(false);

    //saveMetric().then();
    getMetricsFilter();
  }, [submitOk]);

  const getMetricsFilter = (): void => {
    const data: IDataSearchMetric = transformDateSearch(form);
    getMetrics(data);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    submitFn();
  };

  return (
    <form onSubmit={ handleSubmit }>
      <div className={ 'box-select-hour' }>
        <div className={ 'box-select-hour__title' }>
          { form.type.value === 'year' ? 'Seleccione la fecha inicial' : 'Seleccione la fecha' }
        </div>
        <div className={ 'box-select-hour__calendar' }>
          <Calendar
            selectDate={ form.initDate }
            handleSetDay={ (value: string) => setFormValue('initDate', value) }
          />

        </div>

        { form.type.value === 'minute' && (
          <>
            <div className={ 'box-select-hour__time-picker' }>
              <SelectHour
                selectTime={ form.initTimePicker }
                handleSetTime={ (value: string) => setFormValue('initTimePicker', value) }
              />
            </div>
            { form.initDate.error && <p className={ 'error' }>{ form.initDate.error }</p> }
            { (!form.initDate.error && form.initTimePicker.error) &&
              <p className={ 'error' }>{ form.initTimePicker.error }</p> }
          </>
        ) }
      </div>

      { form.type.value === 'day' && (
        <div className={ 'box-select-hour' }>
          <div className={ 'box-select-hour__title' }>Seleccione la fecha final</div>
          <div className={ 'box-select-hour__calendar' }>
            <Calendar
              selectDate={ form.endDate }
              handleSetDay={ (value: string) => setFormValue('endDate', value) }
            />
          </div>
        </div>
      ) }

      <div className={ 'box-select-hour__type' }>
        <p className={ 'box-select-hour__type__title' }>
          Seleccione el tiempo de métrica que quiere:
        </p>
        <select
          className={ 'box-select-hour__type__select' }
          value={ form.type.value }
          onChange={
            (e: React.ChangeEvent<HTMLSelectElement>) => {
              if (e.target.value === 'day') window.location.reload();
              setFormValue('type', e.target.value);
            }
          }
        >
          <option value="day">Día</option>
          <option value="hour">Hora</option>
          <option value="minute">Minuto</option>
        </select>
      </div>

      <div>
        <button className={ 'button button-form' } type="submit">Buscar</button>
      </div>
    </form>
  );
};

export default FormSearchMetrics;
