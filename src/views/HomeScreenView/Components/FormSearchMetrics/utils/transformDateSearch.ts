import { IInitialState } from '../interfaces';
import { IDataSearchMetric } from '../../../../../screens/HomeScreen/interfaces';

export const transformDateSearch = (form: IInitialState): IDataSearchMetric => {
  const type = form.type.value;
  let dateInit = '';
  let dateEnd = '';

  if (type === 'day') {
    console.log('TEST: form.endDate.value => ', form.endDate.value); // TODO console.log

    dateInit = `${ form.initDate.value }T00:00:00.000Z`;
    dateEnd = `${ form.endDate.value }T23:59:59.000Z`;
  } else if (type === 'hour') {
    dateInit = `${ form.initDate.value }T00:00:00.000Z`;
    dateEnd = `${ form.initDate.value }T23:59:59.000Z`;
  } else {
    dateInit = `${ form.initDate.value }T${ form.initTimePicker.value }:00:00.000Z`;
    dateEnd = `${ form.initDate.value }T${ form.initTimePicker.value }:59:59.000Z`;
  }

  return { dateInit, dateEnd, type };
};
