import { IInitialState } from '../interfaces';
import { text } from '../../../../../utils/validators';

export default (form: IInitialState): IInitialState => {
  form.initDate.error = text(form.initDate.value);
  form.initTimePicker.error = text(form.initTimePicker.value, form.type.value === 'minute');
  form.endDate.error = text(form.endDate.value, form.type.value === 'day');
  form.type.error = text(form.type.value);

  if (!form.initDate.error && !form.endDate.error) {
    const initDataTypeDate = new Date(form.initDate.value);
    const endDataTypeDate = new Date(form.endDate.value);

    if (initDataTypeDate > endDataTypeDate) {
      form.endDate.error = 'La fecha seleccionada no puede ser anterior a la inicial';
    }
  }

  return form;
};
