export interface IInitialState {
  name: {
    value: string;
    error: string | null;
  };
  value: {
    value: any; // Ajusta el tipo según tus necesidades
    error: string | null;
  };
  day: {
    value: string;
    error: string | null;
  };
  timePicker: {
    value: string;
    error: string | null;
  };
}

export interface IProps {
  getMetrics: () => void;
}
