import { IInitialState } from '../interfaces';
import { number, text } from '../../../../../utils/validators';

export default (form: IInitialState): IInitialState => {
  form.name.error = text(form.name.value);
  form.value.error = number(form.value.value);
  form.day.error = text(form.day.value);
  form.timePicker.error = text(form.timePicker.value);

  return form;
};
