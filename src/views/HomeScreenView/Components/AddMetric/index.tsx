import React, { useEffect } from 'react';
import { useForm } from '../../../../hooks/useForm';
import { IInitialState, IProps } from './interfaces';
import formValidators from './utils/formValidators';
import Calendar from '../../../../components/Calendar';
import TimePicker from '../../../../components/TimePicker';

import './addMetric.styles.css';
import { IDataAddMetrics } from '../../../../services/api/interfaces/homeApiServiceInterface';
import { addMetric } from '../../../../services/api/homeApiService';

const initialState: IInitialState = {
  name: { value: '', error: null },
  value: { value: '', error: null },
  day: { value: '', error: null },
  timePicker: { value: '', error: null },
};

const AddMetric = ({ getMetrics }: IProps): React.ReactElement => {
  const {
    form, setFormValue, submitFn, submitOk, refreshForm
  } = useForm(initialState, formValidators);

  useEffect(() => {
    if (!submitOk) return;

    saveMetric().then();
  }, [submitOk]);

  const saveMetric = async (): Promise<void> => {
    const data: IDataAddMetrics = {
      name: form.name.value,
      value: form.value.value,
      datetime: `${ form.day.value }T${ form.timePicker.value }:00.000Z`
    };
    await addMetric(data);
    refreshForm();
    getMetrics();
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    submitFn();
  };

  return (
    <>
      <form className={ 'form' } onSubmit={ handleSubmit }>
        <div className={ 'input-name' }>
          <input
            type="text"
            id="name"
            name="name"
            placeholder={ 'Nombre de la métrica' }
            value={ form.name.value }
            onChange={
              (e: React.ChangeEvent<HTMLInputElement>) => setFormValue('name', e.target.value)
            }
          />
          { form.name.error && <p className={ 'error' }>{ form.name.error }</p> }
        </div>

        <div className={ 'input-value' }>
          <input
            type="number"
            id="value"
            name="value"
            placeholder={ 'Valor' }
            value={ form.value.value }
            onChange={
              (e: React.ChangeEvent<HTMLInputElement>) => setFormValue('value', e.target.value)
            }
          />
          { form.value.error && <p className={ 'error' }>{ form.value.error }</p> }
        </div>

        <div className={ 'input-day' }>
          <Calendar
            selectDate={ form.day }
            handleSetDay={ (value: string) => setFormValue('day', value) }
          />
          { form.day.error && <p className={ 'error' }>{ form.day.error }</p> }
        </div>

        <div className={ 'input-time_picker' }>
          <TimePicker
            selectTime={ form.timePicker }
            handleSetTime={ (value: string) => setFormValue('timePicker', value) }
          />
          { form.timePicker.error && <p className={ 'error' }>{ form.timePicker.error }</p> }
        </div>

        <button className={ 'button' } type="submit">Guardar</button>
      </form>
    </>
  );
};

export default AddMetric;
