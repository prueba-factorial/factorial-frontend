import React, { useState } from 'react';

import './actions.css';
import { deleteMetrics, executeSeeder } from '../../../../services/api/homeApiService';
import { IProps } from './interfaces';

const ActionsMetrics = ({ getMetrics }: IProps): React.ReactElement => {
  const [loadingAdd, setLoadingAdd] = useState<boolean>(false);
  const [loadingDelete, setLoadingDelete] = useState<boolean>(false);

  const handleDeleteAll = async (): Promise<void> => {
    if (loadingAdd || loadingDelete) return;

    const confirmation = window.confirm('¿Estás seguro de eliminar todo?');
    if (confirmation) {
      setLoadingDelete(true);
      await deleteMetrics();
      setLoadingDelete(false);
      getMetrics();
    }
  };

  const handleSeeder = async (): Promise<void> => {
    if (loadingDelete || loadingAdd) return;

    const confirmation = window.confirm(
      '¿Estás seguro de añadir 28.800 valores aleatorios comprendidos entre el 1/11/2023 y el' +
      ' 10/11/2023?'
    );

    if (confirmation) {
      setLoadingAdd(true);
      await executeSeeder();
      setLoadingAdd(false);
      getMetrics();
    }
  };

  return (
    <div className={ 'box' }>
      <div className={ 'box__box-text-average' }>
        <p>Se añade 28.800 valores aleatorios, 3 valores ente 1 y 100 cada minuto comprendido entre
          el 1/11/2023
          y
          el 10/11/2023.</p>
      </div>
      <button className={ 'button button-add' } onClick={ handleSeeder }>
        { loadingAdd ? 'Cargando...' : 'Añadir valores aleatorios (28.800 valores)' }
      </button>
      <button className={ 'button button-delete' } onClick={ handleDeleteAll }>
        { loadingDelete ? 'Cargando...' : 'Eliminar todos los valores' }
      </button>
    </div>
  );
};

export default ActionsMetrics;
