import { TMetricsTypes } from '../../../../screens/HomeScreen/interfaces';

interface ChartData {
  data: DataPoint[];
  id: string;
}

interface DataPoint {
  x: number;
  y: number;
}

export interface IProps {
  data: ChartData[];
  width: number;
  height: number;
  min: number | 'auto';
  max: number | 'auto';
  type: TMetricsTypes;
}
