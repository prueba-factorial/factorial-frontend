import { ReactElement } from 'react';
import { Line } from '@nivo/line';
import { IProps } from './interfaces';

const LinealChart = ({ data, width, height, min, max, type }: IProps): ReactElement => {
  return (
    <>
      <Line
        animate
        curve="monotoneX"
        data={ data }
        enablePointLabel
        enableSlices="x"
        height={ height }
        margin={ {
          bottom: 60,
          left: 80,
          right: 20,
          top: 20
        } }
        width={ width }
        xScale={ type === 'day'
          ? {
            format: '%Y-%m-%d',
            precision: 'day',
            type: 'time',
            useUTC: false
          }
          : {
            type: 'linear',
            min: min,
            max: max
          }
        }
        xFormat={ type === 'day' ? 'time:%Y-%m-%d' : '' }
        axisBottom={ type === 'day'
          ? {
            format: '%b %d',
            legend: 'Dias',
            legendOffset: -12,
            tickValues: 'every 2 days'
          }
          : type === 'hour' ? { legend: 'Horas' } : { legend: 'Minutos' }
        }
      />

    </>
  );
};

export default LinealChart;
