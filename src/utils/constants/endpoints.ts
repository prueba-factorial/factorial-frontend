const DEVConfig = {
  request: 'http://localhost:3100'
};

export const metrics = {
  getAverageMetrics: () => `${ DEVConfig.request }/v1/metrics/average`,
  addMetrics: () => `${ DEVConfig.request }/v1/metrics`,
  delete: () => `${ DEVConfig.request }/v1/metrics`,
};

export const seeder = {
  execute: () => `${ DEVConfig.request }/v1/seeder`
};
