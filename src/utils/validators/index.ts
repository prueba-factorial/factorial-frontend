export const text = (
  value: string,
  isRequired: boolean = true
): string | null => {
  if (!isRequired && value === '') return null;
  if (value === '') return 'El valor es requerido';

  return null;
};

export const number = (
  value: string,
  isRequired: boolean = true
): string | null => {
  if (!isRequired && value === '') return null;
  if (value === '') return 'El valor es requerido';

  const numericValue = parseFloat(value);

  if (isNaN(numericValue) || numericValue < 0) {
    return 'Por favor, ingrese un número válido y no negativo';
  }

  return null;
};
