import { IDataSearchMetric } from './interfaces';

export const initialState: IDataSearchMetric = {
  dateInit: '2023-11-01T00:00:00.000Z',
  dateEnd: '2023-11-10T23:59:59.000Z',
  type: 'day'
};
