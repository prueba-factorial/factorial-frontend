import { ReactElement, useEffect, useState } from 'react';
import HomeScreenView from '../../views/HomeScreenView';
import { IDataSearchMetric } from './interfaces';
import { getAverageMetrics } from '../../services/api/homeApiService';
import { initialState } from './initialState';

const HomeScreen = (): ReactElement => {
  const [getMetrics, setGetMetrics] = useState<any>([]);
  // TODO: Pasarlo a un objeto
  const [dataSearchMetric, setDataSearchMetric] = useState<IDataSearchMetric>(initialState);

  const getMetricsApi = async (
    { dateInit, dateEnd, type }: IDataSearchMetric = dataSearchMetric
  ): Promise<void> => {
    setDataSearchMetric({ dateInit, dateEnd, type });
    const result = await getAverageMetrics({ dateInit, dateEnd, type });

    setGetMetrics(result.data);
  };

  useEffect(() => {
    getMetricsApi().then();
  }, []);

  return (
    <HomeScreenView
      metrics={ getMetrics }
      getMetricsApi={ getMetricsApi }
      type={ dataSearchMetric.type }
    />
  );
};

export default HomeScreen;
