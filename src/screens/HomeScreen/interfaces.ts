export type TMetricsTypes = 'day' | 'hour' | 'minute';

export interface ISelectDay {
  value: string;
  error: boolean;
}

export interface ISelectTimePicker {
  value: string;
  error: boolean;
}

export interface IDataSearchMetric {
  dateInit: string
  dateEnd: string;
  type: TMetricsTypes;
}
