import React, { ReactElement } from 'react';
import { CloseButton } from 'react-bootstrap';
import { IProps } from '../TimePicker/interfaces';

import './selectHour.styles.css';

const SelectHour = ({ selectTime, handleSetTime }: IProps): ReactElement => {
  return (
    <div className={ 'container' }>
      <select
        value={ selectTime.value }
        onChange={
          (e: React.ChangeEvent<HTMLSelectElement>) => handleSetTime(e.target.value)
        }
        className={ 'select-hours' }
        style={ selectTime.error ? { borderColor: 'red' } : {} }
      >
        <option key={ 0 } value={ '' }>
          Hora
        </option>
        { [...Array(24)].map((_: number, index: number) => (
          <option key={ index } value={ (index).toString().padStart(2, '0') }>
            { index } h
          </option>
        )) }
      </select>

      { selectTime.value !== '' && (
        <CloseButton
          onClick={ () => handleSetTime('') }
          style={ { marginLeft: '8px' } }
        />
      ) }
    </div>
  );
};

export default SelectHour;
