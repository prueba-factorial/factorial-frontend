import { ISelectTimePicker } from '../../screens/HomeScreen/interfaces';

export interface IProps {
  selectTime: ISelectTimePicker;
  handleSetTime: (time: string) => void;
}
