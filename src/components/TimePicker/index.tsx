import React, { ReactElement } from 'react';
import './timePicker.styles.css';
import { CloseButton } from 'react-bootstrap';
import { IProps } from './interfaces';

const TimePicker = ({ selectTime, handleSetTime }: IProps): ReactElement => {
  return (
    <div className={ 'container' }>
      <input
        type="time"
        value={ selectTime.value }
        onChange={ (e: React.ChangeEvent<HTMLInputElement>) => handleSetTime(e.target.value) }
        style={ selectTime.error ? { borderColor: 'red' } : {} }
      />

      { (selectTime.value !== '' && selectTime.value !== undefined) && (
        <CloseButton
          onClick={ () => handleSetTime('') }
          style={ { marginLeft: '8px' } }
        />
      ) }
    </div>
  );
};

export default TimePicker;
