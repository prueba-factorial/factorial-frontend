import React, { ReactElement } from 'react';
import { IProps } from './interfaces';

const Calendar = ({ selectDate, handleSetDay }: IProps): ReactElement => (
  <div style={ { display: 'flex', alignItems: 'center' } }>
    <input
      type="date"
      value={ selectDate.value }
      onChange={ (e: React.ChangeEvent<HTMLInputElement>) => handleSetDay(e.target.value) }
      style={ selectDate.error ? { borderColor: 'red' } : {} }
    />
  </div>
);

export default Calendar;
