import { ISelectDay } from '../../screens/HomeScreen/interfaces';

export interface IProps {
  selectDate: ISelectDay;
  handleSetDay: (day: string) => void;
}
