import { AxiosResponse } from 'axios';

export interface ApiResponse {
  data: any | null;
  meta: { success: boolean; error: any };
}

export const handleApiResponse = (response: AxiosResponse): ApiResponse => {
  try {
    return {
      data: response.data.data,
      meta: { success: true, error: null }
    };
  } catch (error) {
    return {
      data: null,
      meta: { success: false, error }
    };
  }
};
