import { useState } from 'react';

interface IFormState {
  [key: string]: {
    value: string;
    error: string | null;
  };
}

interface IReturn {
  form: any;
  setFormValue: (name: any, value: string) => void;
  submitFn: () => void;
  submit: boolean;
  submitOk: boolean;
  refreshForm: () => void;
  setSubmitOk: (value: boolean) => void;
}

export const useForm = <T extends IFormState>(
  formInitState: any = {},
  fromValidators: (param: any) => any,
): IReturn => {
  const [form, setForm] = useState<T>(formInitState);
  const [submitOk, setSubmitOk] = useState<boolean>(false);
  const [submit, setSubmit] = useState<boolean>(false);

  const setFormValue = (name: keyof T, value: string): void => {
    setForm({ ...form, [name]: { ...form[name], value } });
    submit && setFormError(
      fromValidators({ ...form, [name]: { ...form[name], value } })
    );
  };

  const setFormError = (newForm: T): void => {
    setForm({ ...form, ...newForm });
  };

  const submitFn = (): void => {
    setSubmit(true);
    setFormError(fromValidators(form));

    const errorsForm = Object.keys(form).filter((key: string) => (
      form[key].error !== null
    ));

    setSubmitOk(errorsForm.length === 0);
  };

  const refreshForm = (): void => {
    setForm(formInitState);
    setSubmitOk(false);
    setSubmit(false);
  };

  return { form, setFormValue, submitFn, submit, submitOk, refreshForm, setSubmitOk };
};

