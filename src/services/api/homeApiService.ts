import { ApiResponse, handleApiResponse } from '../../helpers/apiHelper';
import axios, { AxiosResponse } from 'axios';
import { metrics, seeder } from '../../utils/constants/endpoints';
import { IDataAddMetrics, IParamsGetAverageMetrics } from './interfaces/homeApiServiceInterface';

export const getAverageMetrics = async (params: IParamsGetAverageMetrics): Promise<ApiResponse> => {
  const response: AxiosResponse = await axios.get(metrics.getAverageMetrics(), { params });

  return handleApiResponse(response);
};

export const addMetric = async (data: IDataAddMetrics): Promise<ApiResponse> => {
  const response: AxiosResponse = await axios.post(metrics.addMetrics(), data);

  return handleApiResponse(response);
};

export const deleteMetrics = async (): Promise<ApiResponse> => {
  const response: AxiosResponse = await axios.delete(metrics.delete());

  return handleApiResponse(response);
};

export const executeSeeder = async (): Promise<ApiResponse> => {
  const response: AxiosResponse = await axios.post(seeder.execute());

  return handleApiResponse(response);
};
