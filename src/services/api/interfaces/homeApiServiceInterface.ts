import { TMetricsTypes } from '../../../screens/HomeScreen/interfaces';

export interface IParamsGetAverageMetrics {
  dateInit: string;
  dateEnd: string;
  type: TMetricsTypes;
}

export interface IDataAddMetrics {
  name: string;
  value: number;
  datetime: string;
}
