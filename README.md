# Get start project

- Instalamos las dependencias

```
npm install
# o
yarn install
```

- Iniciamos el proyecto

```
npm start
# o
yarn start
```
